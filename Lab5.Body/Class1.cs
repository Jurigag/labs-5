﻿using Lab5.Body.Implementation;
using Lab5.Body.Contract;
using Lab5.Display.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Body
{
    public class Układ_Wejściowy:IUkładWejściowy
    {
        private IFonia fonia;
        private ISterowanie sterownik;
        private IObraz obraz;
        private IDisplay display;

        private int id;

        public Układ_Wejściowy(IFonia f, ISterowanie s, IObraz o, IDisplay display)
        {
            fonia = f;
            id=0;
            sterownik = s;
            obraz = o;
            this.display = display;
        }

        public void Działam()
        {
            Console.WriteLine("Układ wejściowy działa");
        }

        public void Wyświetlam()
        {
            throw new NotImplementedException();
        }
    }
}
