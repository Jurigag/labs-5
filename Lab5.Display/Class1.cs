﻿using Lab5.Display.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Display
{
    public class DisplayImpl:IDisplay
    {
        private DisplayForm.DisplayForm displayForm;

        public DisplayImpl(DisplayForm.DisplayForm d)
        {
            displayForm = d;
        }

        public void Wyświetlam()
        {
            Console.WriteLine("Wyświetlam");
        }
    }
}
