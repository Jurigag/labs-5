﻿using Lab5.Display.Contract;
using Lab5.DisplayForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab5.Display.Implementation
{
    public class DisplayImpl:IDisplay
    {
        private DisplayForm.DisplayForm displayForm=new DisplayForm.DisplayForm();

        public DisplayImpl()
        {
        }

        public void Wyświetlam(string s)
        {
            var model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                // utworzenie nowej formatki graficznej stanowiącej widok
                var form = new Form();
                // utworzenie modelu widoku (wzorzec MVVM)
                var viewModel = new DisplayViewModel();
                // przypisanie modelu do widoku
                form.DataContext = viewModel;
                // wyświetlenie widoku
                form.Show();
                // zwrócenie modelu widoku do dalszych manipulacji
                return viewModel;
            }), null);

            /* Modyfikacja właściwości modelu - zmiana napisu na wyświetlaczu */
            model.Text = s;
        }
    }
}
