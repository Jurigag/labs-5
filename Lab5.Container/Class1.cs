﻿using PK.Container;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Container
{
    public class PKContainer : IContainer
    {
        private IDictionary<Type, Type> assemblyArray;
        private IDictionary<Type, Type> typeArray;
        private IDictionary<Type, Delegate> delegateArray;
        private IDictionary<Type, object> implArray;
        public PKContainer()
        {
            assemblyArray = new Dictionary<Type, Type>();
            typeArray = new Dictionary<Type, Type>();
            delegateArray = new Dictionary<Type, Delegate>();
            implArray = new Dictionary<Type, object>();
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            {
                Type[] interfaces = typeof(T).GetInterfaces();
                Delegate d = provider;
                if (typeof(T).IsInterface) {
                    delegateArray.Add(typeof(T), d);
                }
                    foreach (var item in interfaces) {
                        delegateArray.Add(item, d);
                    }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            Type t1 = impl.GetType();
            Type[] interfaces = t1.GetInterfaces();
            if (typeof(T) != impl.GetType()) {
                implArray.Add(typeof(T), impl);
            }
                foreach (var item in interfaces) {
                    if (typeof(T) == impl.GetType()) {
                        implArray.Add(item, impl);
                    }
            }
        }

        public void Register(Type type)
        {
            var types = type.GetProperties();
            Type[] interfaces = type.GetInterfaces();
            foreach (var item in interfaces) {
                typeArray.Add(item, type);
            }
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            var objects = assembly.ExportedTypes;
            foreach (var item in objects) {
                if (item.GetInterfaces() != null)
                    foreach (var item2 in item.GetInterfaces()) {
                        assemblyArray.Add(item2, item);
                    }
            }
        }

        public object Resolve(Type type)
        {
            if (typeArray.ContainsKey(type)) {
                if (type.IsInterface) {
                    Type t1;
                    typeArray.TryGetValue(type, out t1);
                    List<object> tab_obj = new List<object>();
                    var properties = t1.GetRuntimeFields();
                    foreach (var item in properties) {
                        Type t2 = item.FieldType;
                        if (t2.IsInterface) {
                            if (typeArray.ContainsKey(t2)){
                                Type t3;
                                typeArray.TryGetValue(t2, out t3);
                                object b = Activator.CreateInstance(t3);
                                tab_obj.Add(b);
                            }
                            else if(assemblyArray.ContainsKey(t2)){
                                Type t3;
                                assemblyArray.TryGetValue(t2, out t3);
                                object b = Activator.CreateInstance(t3);
                                tab_obj.Add(b);
                            }
                            else {
                                throw new UnresolvedDependenciesException();
                            }
                        }
                    }
                    if (tab_obj.Count == 0) {
                        return Activator.CreateInstance(t1);
                    }
                    else {
                        return Activator.CreateInstance(t1, tab_obj.ToArray());
                    }
                }
                return Activator.CreateInstance(type);
            }
            else if (assemblyArray.ContainsKey(type)) {
                if (type.IsInterface) {
                    Type t1;
                    assemblyArray.TryGetValue(type, out t1);
                    List<object> tab_obj = new List<object>();
                    var properties = t1.GetRuntimeFields();
                    foreach (var item in properties) {
                        Type t2 = item.FieldType;
                        if (t2.IsInterface) {
                            if (assemblyArray.ContainsKey(t2)) {
                                Type t3;
                                assemblyArray.TryGetValue(t2, out t3);
                                object b = Activator.CreateInstance(t3);
                                tab_obj.Add(b);
                            }
                            else if (delegateArray.ContainsKey(t2)) {
                                Delegate t3;
                                delegateArray.TryGetValue(t2, out t3);
                                object b = t3.DynamicInvoke();
                                tab_obj.Add(b);
                            }
                            else if (typeArray.ContainsKey(t2)) {
                                Type t3;
                                assemblyArray.TryGetValue(t2, out t3);
                                object b = Activator.CreateInstance(t3);
                                tab_obj.Add(b);
                            }
                            else {
                                throw new UnresolvedDependenciesException();
                            }
                        }
                    }
                    if (tab_obj.Count == 0) {
                        return Activator.CreateInstance(t1);
                    }
                    else {
                        return Activator.CreateInstance(t1, tab_obj.ToArray());
                    }
                }
                return Activator.CreateInstance(type);
            }
            else {
                return null;
            }
        }

        public T Resolve<T>() where T : class
        {
            if (delegateArray.ContainsKey(typeof(T))) {
                Delegate d;
                delegateArray.TryGetValue(typeof(T), out d);
                return (T)d.DynamicInvoke();
            }
            else if (assemblyArray.ContainsKey(typeof(T))) {
                Type t1;
                List<object> tab_obj = new List<object>();
                assemblyArray.TryGetValue(typeof(T), out t1);
                if (!t1.IsInterface) {
                    return (T)Activator.CreateInstance(t1);
                }
                else {
                    var properties = typeof(T).GetProperties();
                    foreach (var item in properties) {
                        Type t2 = item.GetMethod.ReturnType;
                        if (t2.IsInterface) {
                            if (!assemblyArray.ContainsKey(t2)) throw new UnresolvedDependenciesException();
                            else {
                                Type t3;
                                assemblyArray.TryGetValue(t2, out t3);
                                object b = Activator.CreateInstance(t3);
                                tab_obj.Add(b);
                            }
                        }
                    }
                    typeArray.TryGetValue(typeof(T), out t1);
                    if (tab_obj.Count == 0) {
                        return (T)Activator.CreateInstance(t1);
                    }
                    else {
                        return (T)Activator.CreateInstance(t1, tab_obj.ToArray());
                    }
                }
            }
            else if (typeArray.ContainsKey(typeof(T))) {
                Type t1 = typeof(T);
                List<object> tab_obj = new List<object>();
                if (!t1.IsInterface) {
                    return (T)Activator.CreateInstance(t1);
                }
                else {
                    var properties = typeof(T).GetProperties();
                    foreach (var item in properties) {

                        Type t2 = item.GetMethod.ReturnType;
                        if (t2.IsInterface) {
                            if (!typeArray.ContainsKey(t2)) throw new UnresolvedDependenciesException();
                            else {
                                Type t3;
                                typeArray.TryGetValue(t2, out t3);
                                object b = Activator.CreateInstance(t3);
                                tab_obj.Add(b);
                            }
                        }
                    }
                    typeArray.TryGetValue(typeof(T), out t1);
                    if (tab_obj.Count == 0) {
                        return (T)Activator.CreateInstance(t1);
                    }
                    else {
                        return (T)Activator.CreateInstance(t1,tab_obj.ToArray());
                    }
                }
            }
            else if(implArray.ContainsKey(typeof(T))){
                object o1;
                implArray.TryGetValue(typeof(T), out o1);
                return (T)o1;
            }
            else{
                return null;
            }
        }
    }
}
