﻿using Lab5.Body.Contract;
using Lab5.Body.Implementation;
using Lab5.Container;
using Lab5.Display;
using Lab5.Display.Contract;
using Lab5.Display.Implementation;
using System;
using System.Reflection;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(PKContainer);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IUkładWejściowy));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Układ_Wejściowy));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IDisplay));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(DisplayImpl));

        #endregion
    }
}
