﻿using System;
using PK.Container;
using Lab5.Container;
using Lab5.Body.Contract;
using Lab5.Body.Implementation;
using Lab5.Display.Implementation;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            PKContainer container = new PKContainer();
            container.Register(typeof(Układ_Wejściowy));
            container.Register(typeof(DisplayImpl));
            return container;
        }
    }
}
